/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion.clases;

/**
 *
 * @author 1045721591
 */
public class Prestamista {
    
    String nombre;
    float tasa;
    int montoMaximo;

    public Prestamista(){        
    }
    
    public Prestamista(String nombre,float tasa, int montoMaximo){
        this.montoMaximo=montoMaximo;
        this.nombre=nombre;
        this.tasa=tasa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getTasa() {
        return tasa;
    }

    public void setTasa(float tasa) {
        this.tasa = tasa;
    }

    public int getMontoMaximo() {
        return montoMaximo;
    }

    public void setMontoMaximo(int montoMaximo) {
        this.montoMaximo = montoMaximo;
    }
    

    
}
