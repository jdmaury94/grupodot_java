/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion.clases;

/**
 *
 * @author 1045721591
 */
public class Prestamo {
    
    int montoPrestamo;
    int cuotaMensual;
    int pagoTotalCredito;
    float tasaInteresMensual;

    public int getMontoPrestamo() {
        return montoPrestamo;
    }

    public void setMontoPrestamo(int montoPrestamo) {
        this.montoPrestamo = montoPrestamo;
    }

    public int getCuotaMensual() {
        return cuotaMensual;
    }

    public void setCuotaMensual(int cuotaMensual) {
        this.cuotaMensual = cuotaMensual;
    }

    public int getPagoTotalCredito() {
        return pagoTotalCredito;
    }

    public void setPagoTotalCredito(int pagoTotalCredito) {
        this.pagoTotalCredito = pagoTotalCredito;
    }

    public float getTasaInteresMensual() {
        return tasaInteresMensual;
    }

    public void setTasaInteresMensual(float tasaInteresMensual) {
        this.tasaInteresMensual = tasaInteresMensual;
    }
    

    
}
