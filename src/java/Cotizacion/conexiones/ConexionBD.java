/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion.conexiones;

import Cotizacion.clases.Prestamista;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author 1045721591
 */
public class ConexionBD {
        
    private String username="root";
    private String pass="";
    public  String urlConnection="jdbc:mysql://localhost:3306/proyectogd?autoReconnect=true&useSSL=false";
    public  Connection conexion;
    public  Statement stmt;
    public  ResultSet rs;
    
    public ConexionBD(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conexion=DriverManager.getConnection(urlConnection,username,pass);
            stmt=conexion.createStatement();

        }catch(ClassNotFoundException | SQLException e){
            System.out.println(e.getMessage());
        }
    }

    public Connection getConexion(){
        return conexion;
    }
    
    public Prestamista obtenerPrestamista(int montoPrestamo){
        
        Prestamista mejorPrestamista=new Prestamista();
        try {           
            rs=stmt.executeQuery("Select * from prestamista where montoMaximo>="+montoPrestamo+" Order by tasa limit 1");
            //rs=stmt.executeQuery("Select * from prestamista limit 1");
                while(rs.next()){
                    mejorPrestamista.setMontoMaximo(rs.getInt("montoMaximo"));
                    mejorPrestamista.setNombre(rs.getString("nombre"));
                    mejorPrestamista.setTasa(rs.getFloat("tasa"));
                }
            
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
        }
        return mejorPrestamista;        
    }
    
    
    public void inicializarValores(){
        try {
            String query="Select * From prestamista";
            rs=stmt.executeQuery(query);
            if(!rs.isBeforeFirst()){
                //Si no hay datos iniciales los insertamos
                query="Insert into prestamista (nombre,tasa,montoMaximo) VALUES ('Juan',1.5,5000000)";
                stmt.executeUpdate(query);
                
                query="Insert into prestamista (nombre,tasa,montoMaximo) VALUES ('Andrés',2.0,7500000)";
                stmt.executeUpdate(query);
                
                query="Insert into prestamista (nombre,tasa,montoMaximo) VALUES ('María',1.2,3000000)";
                stmt.executeUpdate(query);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void desconectar(){
        try {
              if(stmt!=null)
                 stmt.close();
              if(conexion!=null)
                 conexion.close();
            }catch (SQLException ex) {
               System.out.println(ex.getMessage());;
            }
    }

}
