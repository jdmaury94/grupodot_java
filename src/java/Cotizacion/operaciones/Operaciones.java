/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion.operaciones;

/**
 *
 * @author 1045721591
 */
public class Operaciones {
    
    public static int PLAZO_FIJO=36;
    
    public double getValorAPagar(int montoPrestamo,float tasaInteres){
        float tasaUsuarioPrestamista=tasaInteres/100;
        double valorAPagar=montoPrestamo*(1+PLAZO_FIJO*tasaUsuarioPrestamista);
        
        return valorAPagar;
    }
    
    public double getCuotaMensual(int montoPrestamo,float tasaInteres){
        double valorAPagar=getValorAPagar(montoPrestamo, tasaInteres);
        double cuotaMensual=valorAPagar/PLAZO_FIJO;
        
        return cuotaMensual;
    }
}
