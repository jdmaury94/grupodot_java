/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Cotizacion.servlet;

import Cotizacion.clases.Prestamista;
import Cotizacion.conexiones.ConexionBD;
import Cotizacion.operaciones.Operaciones;
import java.io.IOException;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author 1045721591
 */
public class CalcularCotizacion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
            response.setContentType("application/x-www-form-urlencoded");
            //response.setContentType("text/html");
            if(request.getMethod().equals("POST"))
            {
                String mensaje="";
                ConexionBD con=new ConexionBD();

                try{
                        int montoPrestamo=Integer.parseInt(request.getParameter("montoPrestamo"));
                        Operaciones operacion=new Operaciones();                    
                        con.inicializarValores();
                        Prestamista prestamista=con.obtenerPrestamista(montoPrestamo);
                        DecimalFormat df=new DecimalFormat("##.00");

                        if(prestamista.getNombre()!=null){ //Si encontró al menos un registro
                            request.setAttribute("montoPrestamo", request.getParameter("montoPrestamo"));
                            request.setAttribute("nombrePrestamista", prestamista.getNombre());
                            request.setAttribute("tasaInteres", prestamista.getTasa());
                            request.setAttribute("cuotaMensual", df.format(operacion.getCuotaMensual(montoPrestamo,prestamista.getTasa())));
                            request.setAttribute("valorAPagar", df.format(operacion.getValorAPagar(montoPrestamo, prestamista.getTasa())));
                        }
                        else
                            mensaje="No se encontró ningún prestamista con esas condiciones";
                    }catch(NumberFormatException ex){
                                mensaje="Verifique la información ingresada!";
                           }
                    request.setAttribute("mensaje", mensaje);
                    request.getRequestDispatcher("mejorCotizacion.jsp").forward(request, response);            
                    con.desconectar();
            }
            else
                response.setStatus(405);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
