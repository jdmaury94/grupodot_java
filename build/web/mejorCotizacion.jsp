<%-- 
    Document   : mejorCotizacion
    Created on : 18/09/2018, 11:31:19 PM
    Author     : 1045721591
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Calcular cotización</h1>
        <form name="formCotizacion" action="CalcularCotizacion" method="POST">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <text>Monto préstamo </text>
                        </td>
                        <td>
                            <input type="int" name="montoPrestamo" value="${montoPrestamo}"  /><br> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <text>Valor a pagar </text>
                        </td>
                        <td>
                            <input type="text" value="${valorAPagar}"  disabled="true"/>
                        </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <input type="submit" value="Calcular cotizacion"/> 
        </form>
        <br>
        
        <table border="2">
            <tr>
                <td><b>Socio que realiza el préstamo</b></td>
                <td><b>Cuota Mensual</b></td>
                <td><b>Pago total del crédito</b></td>
                <td><b>Tasa de interés mensual</b></td>
            </tr>
            <tr>
                <td>${nombrePrestamista}</td>
                <td>$ ${cuotaMensual}</td>
                <td>$ ${valorAPagar}</td>
                <td>${tasaInteres}%</td>
            </tr>

        </table>
        <h1><p>${mensaje}</p></h1>
       
    </body>
</html>
